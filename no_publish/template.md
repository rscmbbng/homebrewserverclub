Title: This is a template for a post
Date: 2016-5-14
Category: log
Tags: try-out
Slug: the-template-post

This is a template you can use with a short description of some of the syntax.

*bold* **italics**

<!-- PELICAN_END_SUMMARY -->  

use the above to choose where the text is cut-off

images:
![image description]({filename}images/myimage.png)

urls:
[http://homebrewserver.club/](This is our webpage)


references in text:

hello I need to be referenced[ref] this creates a numbered list at the bottom of the page, not bad no? it can be styles in the [css](http://homebrewserver.club/theme/css/main.css) by addressing the class simple-footnotes[/ref]

