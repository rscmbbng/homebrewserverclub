Title: A Longer Text
Date: 2017-2-10
Category: test
Tags: 
Slug: a-longer-text


FBI Inteviewed Flynn in Trump's First Days in Office, Officials Say
---

![]({filename}/images/nyt.jpg)


WASHINGTON — F.B.I. agents interviewed Michael T. Flynn when he was national security adviser in the first days of the Trump administration about his conversations with the Russian ambassador, current and former officials said on Tuesday.

The interview raises the stakes of what so far has been a political scandal that cost Mr. Flynn his job. If he was not entirely honest with the Federal Bureau of Investigation, it could expose Mr. Flynn to a felony charge. President Trump asked for Mr. Flynn’s resignation on Monday night.

While it is not clear what he said in his F.B.I. interview, Mr. Flynn maintained publicly for more than a week after his interview that his conversations with the ambassador had been innocuous and did not involve Russian sanctions, something now known to be false.

Shortly after the F.B.I. interview, on Jan. 26, the acting attorney general, Sally Q. Yates, told the White House that Mr. Flynn was vulnerable to Russian blackmail because of inconsistencies between what he had said publicly and what intelligence officials knew to be true [ref] Hint: nothing is true [/ref].

At issue is a conversation during the presidential transition in which Mr. Flynn spoke to the Russian ambassador about sanctions levied against Russia by the Obama administration. The call spurred an investigation by the F.B.I. into whether Mr. Flynn had violated the rarely invoked Logan Act, which prohibits private citizens from negotiating with foreign governments in disputes with the United States.

The National Security Agency routinely eavesdrops on calls involving high-ranking foreign diplomats. Mr. Flynn was not a focus of the eavesdropping, officials said.

It is not clear whether Mr. Flynn had a lawyer for his interview with the F.B.I. or whether anyone at the White House, including lawyers there, knew the interview was happening.

Document | Michael Flynn’s Resignation Letter Michael T. Flynn, under scrutiny for his communication with Russia, resigned as President Trump's national security adviser late Monday.

Sean Spicer, the White House press secretary, said on Tuesday that President Trump was made aware of the situation weeks ago. Mr. Spicer said the White House had reviewed the situation and determined that Mr. Flynn didn’t violate any laws during his call with the Russian ambassador.[ref] He did violate the world's trust by deliberately lying to the press[/ref]

Mr. Spicer said Mr. Flynn was asked to resign because he had lost the trust of the president and vice president.

In late December, Mr. Flynn spoke with Sergey Kislyak, Russia’s ambassador to the United States. During the call, Mr. Flynn and the ambassador discussed sanctions, according to current and former United States officials. In the call, Mr. Flynn indicated that the Obama administration was Moscow’s adversary and that relations would change under Mr. Trump.

But on Jan. 14, Mr. Flynn told Vice President Mike Pence that he hadn’t discussed sanctions in his call. The next day, Mr. Pence went on “Fox News Sunday” and declared: “I talked to General Flynn yesterday, and the conversations that took place at that time were not in any way related to the new U.S. sanctions against Russia or the expulsion of diplomats.”

Even after F.B.I. agents later interviewed Mr. Flynn and Ms. Yates warned the White House, Mr. Flynn denied yet again — this time to The Washington Post on Wednesday — that he had discussed sanctions with the Russian ambassador.


