Title: About
Date: 2016-04-10 
Category: info
Subcategory: about
Tags: homebrewserver
Slug: about


A monthly gathering for those who (wish to) host their own online services from home, rather than using commercial and privacy unfriendly alternatives. Together we config and work on our home brew servers. These are low-cost, low-powered, low-maintenance, high-fun computers through which wecan host all of our online necessities and keep them out of the cloud. As we gain more knowledge about what software to use in what situation we write and publish guides for others to share. 

If you have questions or would like to join check out our [mailinglist](http://lurk.org/groups/hsc/) or join us in our [XMPP chatroom](xmpp://homebrewserver.club@muc.lurk.org)

0/ salutations
