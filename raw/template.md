Title: This is a template for a post
Date: 2017-2-10
Category: pelican
Tags: 
Slug: the-template-post

This is a template you can use with a short description of some of the syntax.

*italics* **bold**

<!-- PELICAN_END_SUMMARY -->  

use the above to choose where the text is cut-off

images:
![image description]({filename}images/myimage.png)

urls:
[http://homebrewserver.club/](This is our webpage)


references in text:

hello I need to be referenced[ref] this creates a numbered list at the bottom of the page, not bad no? it can be styles in the [css](http://homebrewserver.club/theme/css/main.css) by addressing the class simple-footnotes[/ref]

Code syntax
Python:

    :::python
    print("homebrewserver.club")

Python with line numbers:

	#!python
	print("line1")

Bash:

   #!/bin/bash
   rm -rf /

